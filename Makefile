all: xenial bionic

ifeq "$(PROGRESS)" "1"
SHOWP = "--progress=plain"
endif

NPROC = 20
DEST_DIR = $(HOME)/panda/
REGISTRY = registry.gitlab.com/wideglide/panda-docker

build_bionic: bionic/Dockerfile
	DOCKER_BUILDKIT=1 docker build --target builder -t panda_build:18.04 bionic
	DOCKER_BUILDKIT=1 docker build --target install_panda -t panda_panda:18.04 bionic
	DOCKER_BUILDKIT=1 docker build --target install_pypanda -t panda_pypanda:18.04 bionic

bionic: build_bionic
	DOCKER_BUILDKIT=1 docker build -t panda:18.04 bionic

test_bionic: build_bionic
	docker run --rm -it --privileged --workdir /panda/build panda_build:18.04 make -j$(NPROC) check

test_bionic_pypanda: bionic
	./taint_unit_tests.sh i386 18.04



build_focal: focal/Dockerfile
	DOCKER_BUILDKIT=1 docker build --target builder -t panda_build:20.04 focal
	DOCKER_BUILDKIT=1 docker build --target install_panda -t panda_panda:20.04 focal
	DOCKER_BUILDKIT=1 docker build --target install_pypanda -t panda_pypanda:20.04 focal

focal: build_focal
	DOCKER_BUILDKIT=1 docker build -t panda:20.04 focal

test_focal: build_focal
	docker run --rm -it --privileged --workdir /panda/build panda_build:20.04 make -j$(NPROC) check

test_focal_pypanda: focal
	./taint_unit_tests.sh i386 20.04



build_xenial: xenial/Dockerfile
	DOCKER_BUILDKIT=1 docker build --target builder -t panda_build:16.04 $(SHOWP) xenial
	DOCKER_BUILDKIT=1 docker build --target install_panda -t panda_panda:16.04 $(SHOWP) xenial
	DOCKER_BUILDKIT=1 docker build --target install_pypanda -t panda_pypanda:16.04 $(SHOWP) xenial

xenial: build_xenial
	DOCKER_BUILDKIT=1 docker build -t panda:16.04 xenial

test_xenial: build_xenial
	docker run --rm -it --privileged --workdir /panda/build panda_build:16.04 make -j$(NPROC) check

test_xenial_pypanda: xenial
	./taint_unit_tests.sh i386 16.04

install_xenial:
	rm -rf $(DEST_DIR)/build
	docker create --name panda_xenial panda_build:16.04
	docker cp panda_xenial:/panda/build $(DEST_DIR)
	docker cp panda_xenial:/usr/local/lib/libdwarf.so.1.0.0 $(DEST_DIR)/build
	docker rm panda_xenial

install2:
	rm -rf $(DEST_DIR)/bulid/install
	mkdir -p $(DEST_DIR)/build/install
	docker run --rm -t -v $(DEST_DIR)/build/install:/usr/local panda_build:16.04 make -C /panda/build install

lint:
	hadolint bionic/Dockerfile
	hadolint focal/Dockerfile
	hadolint xenial/Dockerfile



# To manually push docker containers, run make push after doing a docker login to github registry
push: bionic_push xenial_push focal_push


bionic_push: bionic
	docker tag panda:18.04 $(REGISTRY)/panda:18.04
	docker push $(REGISTRY)/panda:18.04

focal_push: focal
	docker tag panda:20.04 $(REGISTRY)/panda:20.04
	docker push $(REGISTRY)/panda:20.04

xenial_push: xenial
	docker tag panda:16.04 $(REGISTRY)/panda:16.04
	docker push $(REGISTRY)/panda:16.04
