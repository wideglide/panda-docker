# Panda Docker

_smaller_ Docker images for [panda](https://github.com/panda-re/panda)

6.5GB >> 1.25GB functional pypanda Docker image

Re-wrote the multi-stage Dockerfiles for Panda enabling small docker images,
also useful for compiling and installing on a host system.
- 3 stage image
- enabled buildkit for faster builds
- reduced the number of layers
- linting via [hadolint](https://github.com/hadolint/hadolint)
- enabled numa in docker via `--privileged`
- update `PANDA_GITREF` build arg to override caching and build with current source
- converted ENV variables to build ARGs


## Build and test
- `make test_bionic` will build the images and run `make check`
- `make test_bionic_pypanda` to run the pypanda taint2 unit tests,
   *requires* local qcow2 images

## Pull docker image
- `docker pull registry.gitlab.com/wideglide/panda-docker/panda:18.04`
- `docker pull registry.gitlab.com/wideglide/panda-docker/panda:16.04`


## Docker image descriptions

**panda_build** : _stage1_ Panda fully built, no installs completed

**panda_panda** : _stage2a_ Panda installed via `make install`

**panda_pypanda** : _stage2b_ Panda installed via `python3 setup.py install`

**panda** : _stage3_ PyPanda installed in a minimal container


| name           | TAG     | SIZE   | notes  |
| -------------- | ------- | :----: | ------ |
| panda_build    | 16.04   | 3.46GB | stage 1 |
| panda_panda    | 16.04   | 3.79GB | stage 2a |
| panda_pypanda  | 16.04   | 5.10GB | stage 2b |
| panda          | 16.04   | 1.16GB | stage 3 |
| panda_build    | 18.04   | 3.32GB | stage 1 |
| panda_panda    | 18.04   | 3.65GB | stage 2a |
| panda_pypanda  | 18.04   | 4.97GB | stage 2b |
| panda          | 18.04   | 1.22GB | stage 3 |
| pandare/panda  | latest  | 6.48GB | original image |

