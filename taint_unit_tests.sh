#!/bin/bash

host_path="/home/panda/regdir/qcows"
docker_path="/home/panda/regdir/qcows"
wheezy_qcow2="wheezy_panda2.qcow2"
bionic_qcow2="bionic-server-cloudimg-amd64-noaslr-nokaslr.qcow2"

TARGET="${1:-i386}"
TAG="${2:-18.04}"
DOCKER_IMG="panda:${TAG}"

docker run --name panda_test \
        -v ${host_path}/${wheezy_qcow2}:${docker_path}/${wheezy_qcow2} \
        -v ${host_path}/${bionic_qcow2}:${docker_path}/${bionic_qcow2} \
        --rm -t ${DOCKER_IMG} bash -ec \
"git clone --depth=1 https://github.com/panda-re/panda_test /panda_test;
cd /panda_test/tests/taint2;
python3 taint2_multi_arch_record_or_replay.py --arch ${TARGET} --mode record;
python3 taint2_multi_arch_record_or_replay.py --arch ${TARGET} --mode replay;
if sed -i '/^\s*$/d' taint2_log; then echo 'Taint unit test log found!'; else echo 'Taint unit test log NOT found!'; exit 1; fi;
printf '\n[*] Passed ';
grep -c pass taint2_log | tr -d '\n';
printf ' tests.\n    Failures:  ';
if grep -i 'fail' taint2_log; then echo 'TEST FAILED!' && exit 1; else echo -e 'None.\nTEST PASSED!' && exit 0; fi"
