#!/bin/bash -eu

DISTRO=xenial
PYTHON_VER="python3.7"
PYTHON_VENV="${HOME}/.virtualenvs/pypanda"
PYTHON_KEY="https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x6A755776"
PYTHON_PPA="http://ppa.launchpad.net/deadsnakes/ppa/ubuntu"
LIBDWARF_GIT="https://git.code.sf.net/p/libdwarf/code" 
TARGET_LIST="x86_64-softmmu,i386-softmmu,arm-softmmu,ppc-softmmu,mips-softmmu,mipsel-softmmu"
PANDA_GIT="https://github.com/panda-re/panda.git"
PANDA_GITREF="d68f846"
PANDA_KEY="https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xF611E3F5"
PANDA_PPA="http://ppa.launchpad.net/phulin/panda/ubuntu"
TOOLCHAIN_R_KEY="https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xBA9EF27F"
TOOLCHAIN_R_PPA="http://ppa.launchpad.net/ubuntu-toolchain-r/test/ubuntu"
CFFI_PIP="https://foss.heptapod.net/pypy/cffi/-/archive/branch/default/cffi-branch-default.zip"

sudo apt-get -qq update && \
DEBIAN_FRONTEND=noninteractive sudo apt-get -qq install -y --no-install-recommends \
  apt-transport-https \
  ca-certificates \
  gnupg \
  wget && \
wget -qO- "${PYTHON_KEY}" | sudo apt-key add - && \
echo "deb ${PYTHON_PPA} ${DISTRO} main " | sudo tee /etc/apt/sources.list.d/deadsnakes.list && \
wget -qO- "${TOOLCHAIN_R_KEY}" | sudo apt-key add - && \
echo "deb ${TOOLCHAIN_R_PPA} ${DISTRO} main" | sudo tee /etc/apt/sources.list.d/toolchain_ppa.list && \
wget -qO- "${PANDA_KEY}" | sudo apt-key add - && \
echo "deb ${PANDA_PPA} xenial main" | sudo tee /etc/apt/sources.list.d/panda_ppa.list && \
sudo apt-get -qq update && \
DEBIAN_FRONTEND=noninteractive sudo apt-get -qq install -y --no-install-recommends \
  build-essential \
  chrpath \
  gcc-multilib \
  genisoimage \
  git \
  libaio1 \
  libbluetooth3 \
  libbrlapi0.6 \
  libcacard0 \
  libcap-ng0 \
  libcapstone3 \
  libcurl3-gnutls \
  libelf1 \
  libfdt1 \
  libglib2.0-0 \
  libgnutls30 \
  libiscsi2 \
  libjpeg-turbo8 \
  libjpeg8 \
  libnettle6 \
  libnuma1 \
  libpixman-1-0 \
  libpng12-0 \
  libprotobuf-c1 \
  libprotobuf9v5 \
  lib${PYTHON_VER}-dev \
  librados2 \
  librbd1 \
  librdmacm1 \
  libsasl2-2 \
  libsasl2-modules-db \
  libsdl1.2debian \
  libspice-server1 \
  libusb-1.0-0 \
  libusbredirparser1 \
  libwiretap8 \
  libxen-4.6 \
  libxenstore3.0 \
  nasm \
  ${PYTHON_VER}-venv

if [ ! -e ${PYTHON_VENV}/bin/activate ]; then
    echo ${PYTHON_VER} -m venv ${PYTHON_VENV}
    ${PYTHON_VER} -m venv ${PYTHON_VENV}
fi

source ${PYTHON_VENV}/bin/activate
${PYTHON_VER} -m pip install --upgrade --no-cache-dir pip 
${PYTHON_VER} -m pip install --upgrade --no-cache-dir setuptools wheel
${PYTHON_VER} -m pip install --upgrade --no-cache-dir pycparser protobuf "${CFFI_PIP}" colorama 
